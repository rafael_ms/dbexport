# DB Export Campaing

##Image Preloader

var Preload = require('imagepreloader');
var Canvas = require('canvas');
var Image = Canvas.Image;
var imgload = new Preload({Image:Image});

var srcImg =  path.join(__dirname,'client/public/images/');
imgload
    .add(path.join(srcImg,'4th/machine.png'))
    .add(path.join(srcImg,'4th/machine-back.png'))
    .add(path.join(srcImg,'4th/glass.png'))
    .add(path.join(srcImg,'4th/sand.png'))
    .add(path.join(srcImg,'4th/sand-bottom.png'))
    .add(path.join(srcImg,'4th/background.jpg'))
    .add(path.join(srcImg,'2nd/header.png'))
    .add(path.join(srcImg,'2nd/details.png'))
    .add(path.join(srcImg,'1st/footer-top.png'))
    .add(path.join(srcImg,'common/PledgeOMETER.png'))
    .success(function(images) {console.log(images)})
    .error(function(msg) { console.log("Error:" + msg) })
    .done();