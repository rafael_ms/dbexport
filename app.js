var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var errorHandler = require('errorhandler');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var helmet = require('helmet');
var compression = require('compression');
var async = require('async');

var Middleware = require('./server/misc/middleware');

var sass = require('node-sass-middleware'); 
var uglifyMiddleware = require('express-uglify-middleware');


var app = express();

// view engine setup
app.set('views', path.join(__dirname, '/client/views'));
app.set('view engine', 'jade');

/**
 *  SASS & JS Uglify
 */

/*****************************************/
var destPathcss =  path.join(__dirname,'client/public/stylesheets/');
var srcPathcss = path.join(__dirname,'precomp/sass/');
app.use('/stylesheets',express.static(path.join(__dirname, 'client/public')));

var destPathjs =  path.join(__dirname,'client/public/javascripts');
var srcPathjs = path.join(__dirname,'precomp/js');
app.use('/javascripts',express.static(path.join(__dirname, 'client/public')));

/*Async Load Middlewares*/

function parallel(middlewares) {
  return function (req, res, next) {
    async.each(middlewares, function (mw, cb) {
      mw(req, res, cb);
    }, next);
  };
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(favicon(path.join(__dirname, 'client/public', 'favicon.ico')));

app.use(parallel([
    sass({
           src: srcPathcss
         , dest: destPathcss
         , prefix:  '/stylesheets'
         , outputStyle: 'compressed'
         , response: false
         , debug: false
    }),
    uglifyMiddleware({
         src:  srcPathjs
        ,dest: destPathjs
        ,prefix: "/javascripts"
        ,compressFilter: /\.js$/
        ,compress: true
        ,force: false
        ,debug: false
    }),
    logger('combined'),
    cookieParser(),
    express.static(path.join(__dirname, 'client/public')),
    helmet({frameguard: false }),
    Middleware.expressValidator,
    compression()
]));

app.use(errorHandler({ dumpExceptions: true, showStack: true }));


require('./server/routes/routes')(app);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.json({
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: {}
  });
});

module.exports = app;
