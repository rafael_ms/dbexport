//IE doesn't support ES6 =>, use normal function() ..

angular.module('dbexport', ['ngRoute']).controller('mainController', function($scope, $http) {
  $scope.formData = {};
  $scope.pledge = function() {
    track('Frames','Frame 2 - Form', 'Submit Clicked');
    // create angular controller
    // function to submit the form after all validation has occurred
    // check to make sure the form is completely valid
    if ($scope.pledgeForm.$valid) {
        track('Frames','Frame 2 - Form', 'Valid Fields');
        $http.post('/', $scope.formData)
          .then(function(response) {
            if(response.data == true){
              nextFrame('.frame.frameFour, .frame.frameThree','hide', 'active');
              nextFrame('.frame.frameTwo','active', 'off');
              track('Frames','Frame 3 - Sand Machine', 'Loading');
            }
            else{
              //write return function to false results
              track('Frames','Frame 2 - Form', 'Invalid Fields');
              formStatus(response.data[1],response.data[2]);
              errorHighlight(response.data[2]);
              console.log('Error: ' + JSON.stringify(response.data));
            }
          })
          .catch(function(error) {
            track('Frames','Frame 2 - Form', 'Error');
            console.log('Error: ' + JSON.stringify(error));
          });
    }
    else
      track('Frames','Frame 2 - Form', 'Invalid Fields');
  };
});