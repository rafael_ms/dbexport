const pg = require('pg');
const connectionString = process.env.DATABASE_URL || 'postgres://localhost:5432/exportdb';

var config = require('../config/config');

const client = new pg.Client(config.db);
client.connect();

var extension = "CREATE EXTENSION CITEXT;";

var table = ' CREATE TABLE pledges( '
							+ '  id SERIAL PRIMARY KEY '
							+ ', name VARCHAR(350) NOT NULL'
							+ ', email CITEXT NOT NULL'
							+ ', phone VARCHAR(12) NOT NULL'
							+ ', submitted timestamp with time_zone);'
							+ "  ALTER TABLE pledges ALTER COLUMN submitted SET DEFAULT now() AT TIME ZONE 'NZDT';" ;

var procedure = "CREATE OR REPLACE FUNCTION add_pledge(pName VARCHAR(350), pEmail CITEXT, pPhone VARCHAR(12))"
    			+ "	RETURNS void AS $$"
    			+ "BEGIN"
    			+ "INSERT INTO pledges(name, email, phone) values(pName, pEmail, pPhone);"
    			+ "END;"
    			+ "$$ LANGUAGE plpgsql;"

const query = client.query( extension + table + procedure );

query.on('end', () => { client.end(); });



