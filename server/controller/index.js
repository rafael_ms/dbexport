var utils = require('../misc/utils');
var config = require('../config/config');

exports.home = function(request, response){
	var images = ['http://i.imgur.com/flVf3Z7.jpg','http://i.imgur.com/flVf3Z7.jpg'];
	var image = images[Math.floor(Math.random()*images.length)];
	var Fshare = "https://www.facebook.com/dialog/feed?app_id=382170612156510&link=http://www.dbexportbeer.co.nz/&picture=" + image + "&name=Drink%20DB%20Export%20and%20help%20save%20our%20beaches.%20&caption=DB%20Export&description=Pledge%20your%20support%20to%20be%20in%20to%20win.%20By%20pledging%20your%20support%20you’re%20helping%20us%20make%20DB%20Export%20Beer%20Bottle%20Sand,%20sand%20made%20from%20crushed%20glass.";
    var Tshare = " https://twitter.com/share?url=http://www.dbexportbeer.co.nz/&text=Drink%20DB%20Export%20and%20save%20our%20beaches.%20";
    
    response.render('index', {
		    title: 'DB Export',
		    facebook: Fshare,
		    twitter: Tshare,
		    analytics: config.analytics.script,
		    CDN:config.CDN.path
		});
};

exports.pledge = function(request, response){
    const data = {   name: request.fields.name
    			   , email: request.fields.email
    			   , phone: request.fields.phone
    			 };
	// Get a Postgres client from the connection pool
	var sql = 'SELECT add_pledge($1, $2, $3)';
	var params = [data.name, data.email, data.phone];

	try{
	   	utils.connect( response, sql, params );
	   	return response.json(true);
	}
	catch(error){
		utils.log('debug', 'controller', JSON.stringify(error), '- DB Error');
	}
};

exports.terms = function(request,response){
	response.render('terms-and-conditions', {
		    title: 'DB Export'
		});
}


