const pg = require('pg');
const connectionString = process.env.DATABASE_URL || 'postgres://localhost:5432/exportdb';
var config = require('../config/config');

exports.connect = function(res, sql, params){
  const results = [];
  pg.defaults.ssl = false;
  pg.connect(config.db, (err, client, done) => {
      // Handle connection errors
      if(err) {
        done();
        console.log(err);
        return res.status(500).json({success: false, data: err});
      }
      // SQL Query > Insert Data
      const query = client.query(sql, params);
      // SQL Query > Select Data

      // After all data is returned, close connection and return results
      query.on('end', () => {
        done();
      });
  });
}

exports.log = function(severity, key, val, text) {
    text = text || '';

    if (typeof (console[severity]) === 'function') {
      console[severity]('[log]  \x1B[90m%s:\x1B[0m \x1B[36m%s %s\x1B[0m', key, val, text);
    } else {
      console.error('[log]  \x1B[90m%s:\x1B[0m \x1B[36m%s %s\x1B[0m', key, val, text);
    }
  };