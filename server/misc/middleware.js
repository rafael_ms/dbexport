var Validation = require('./validations');
var Validator = require('validator');
var config = require('../config/config');
var utils = require('./utils');

exports.expressValidator = function(request, response, next){
	if (request.url == '/' && request.method === 'POST') {
	    var valid = [];
	    var fields = request.body; // fields from post

	   try{
	   	valid = validate(fields);
	   }
	   catch(e){
			response.end( e );
		}

	   if(valid[0]){
	   	request.fields = valid[1];
	   	next();
	   }
	   else{
	   	response.writeHead(response.statusCode.toString());
	   	response.end( JSON.stringify([ valid[0], valid[1], valid[2] ]));
	   }
	}
	else
		next();

}

function validate(fields){
	var errors = 0;
	var result = [true, fields];
	var requiredFields = config.form.required;

	//utils.log('debug', 'middlaware', JSON.stringify(fields), '');

	if( Object.keys(fields).length < config.form.minimumFields )
		errors++;

	if(errors > 0)
		return [false, "Invalid Submission!", "#10001"];

	for(key in fields){
		if(requiredFields.indexOf(key) != -1){
			if(config.form.skip.indexOf(key) == -1){
				if(Validation.IsNullOrEmpty(fields[key]))
					errors++;
				fields[key] = Validator.escape(fields[key]);
			}
		}
	}

	if(errors > 0)
		return [false, "Check the required fields!", "#10002"];

	if(!Validator.isEmail(fields.email))
		errors++;
	fields.email = Validator.normalizeEmail(fields.email,[{all_lowercase: true}]);

	if(errors > 0)
		return[false, "Check your email!", "#10003"];

	if(!Validator.isMobilePhone(fields.phone,'en-NZ'))
		errors++;

	if(!Validator.isNumeric(fields.phone))
		errors++;

	if(errors > 0)
		return [false, "Invalid phone number!", "#10004"];

	//utils.log('debug', 'middlaware', JSON.stringify(fields), '');

	return result;
}

/// Errors
/// #10001 - Invalid amount of fields submitted
/// #10002 - Empty Fields
/// #10003 - Invalid email address
/// #10004 - Invalid Phone number
