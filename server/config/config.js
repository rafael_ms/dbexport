//Source
//http://stackoverflow.com/questions/27026619/where-do-i-put-database-connection-information-in-a-node-js-app
//https://github.com/narendrasoni1989/BlogApp
//http://learn.mean.io/
//https://www.airpair.com/postgresql/posts/sql-vs-nosql-ko-postgres-vs-mongo

var path = require("path");
var extend = require("util")._extend;

var development = require("./env/development");
var test = require("./env/test");
var production = require("./env/production");

var defaults = {
   root: path.normalize(__dirname + '/..')
};

module.exports = {
   development: extend(development,defaults),
   test: extend(test,defaults),
   production: extend(production,defaults)
}[process.env.NODE_ENV || "development"]