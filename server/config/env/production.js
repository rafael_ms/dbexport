module.exports = {
    db : {
	  user: 'aovnolghtvsnrw', //env var: PGUSER 
	  database: 'd6ek5qk2bfsh18', //env var: PGDATABASE 
	  password: 'e4a8596b8d2accdadaa187a9629bf6e892700951c933ee1dcbbe6357957b02aa', //env var: PGPASSWORD 
	  host: 'ec2-54-225-236-102.compute-1.amazonaws.com', // Server hosting the postgres database 
	  port: 5432, //env var: PGPORT 
	  idleTimeoutMillis: 30000, // how long a client is allowed to remain idle before being closed 
	},
	form:{
		  minimumFields: 4
		, required: ['name','email', 'phone', 'terms']
		, skip: ['terms']
	},
	analytics: {
		script: "<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga'); ga('create', 'UA-92478709-1', 'auto'); ga('send', 'pageview'); </script>"
	},
	CDN : {
		path: "https://dhxvclx0xnusp.cloudfront.net/"
	}
};