module.exports = {
    db : {
	    user: 'postgres' //env var: PGUSER 
	  , database: 'exportdb' //env var: PGDATABASE 
	  , password: '159753' //env var: PGPASSWORD 
	  , host: 'localhost' // Server hosting the postgres database 
	  , port: 5432 //env var: PGPORT 
	  , max: 10 // max number of clients in the pool 
	  , idleTimeoutMillis: 30000 // how long a client is allowed to remain idle before being closed 
	},
	form:{
		  minimumFields: 4
		, required: ['name','email', 'phone', 'terms']
		, skip: ['terms']
	},
	analytics: {
		script: ""
	},
	CDN : {
		path: ""
	}
};