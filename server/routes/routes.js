var index = require('../controller/index');

/* GET home page. */
module.exports = function(app){
		app.get('/', index.home);
		app.post('/', index.pledge);
}
