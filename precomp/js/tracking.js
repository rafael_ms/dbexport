$(function(){
	track = function(category, action, label){
		if(typeof ga == 'function')
			ga('send', {
			  hitType: 'event',
			  eventCategory: category,
			  eventAction: action,
			  eventLabel: label
			});
	}
});