$(function(){
    var rand = '';
    //Click Tracking
    $(document).on('click',function(e){
        $target = $(e.target);

        if($target.hasClass('footer-facebook-click'))
            track('Sharing','Footer', 'Facebook Share Click');
        if($target.hasClass('footer-twitter-click'))
            track('Sharing','Footer', 'Twitter Share Click');
        if($target.hasClass('frame-facebook-click'))
            track('Sharing','Frame 4 - Last Frame', 'Facebook Share Click');
        if($target.hasClass('frame-twitter-click'))
            track('Sharing','Frame 4 - Last Frame', 'Twitter Share Click');
        if($target.hasClass('overlay'))
            track('Frames','Preloader', 'Preloader Click');
        if($target.hasClass('frameOne'))
            track('Frames','Frame 1', 'Click');
        if($target.hasClass('frameTwo'))
            track('Frames','Frame 2 - Form', 'Click');
        if($target.hasClass('frameThree')){
            track('Frames','Frame 4 - Last Frame', 'Click');
            window.open(impressionTracker(true), '_blank');
        }
        if($target.hasClass('frameFour'))
            track('Frames','Frame 3 - Sand Machine', 'Click');
    });

    $(document).ready(function(){
        //impression tracker
        var imptrk = document.createElement('img');
        trackerRandom();
        imptrk.src = impressionTracker();
        imptrk.className = 'hide';
        $('body').prepend(imptrk);


        setTimeout(function(){
            if($('.overlay').length > 0)
                nextFrame('.overlay svg','','animated zoomOut');
                track('Frames','Unloading in Process', 'Prealoder');
        },4500);
    });

    trackerRandom = function(){
        rand = Math.round(Math.random() * 10000000000);
    }

    impressionTracker = function(click){
        if(click == true)
            return "http://data.apn.co.nz/apnnz/adclick/FCID=163284/relocate=https://ad.doubleclick.net/ddm/trackclk/N7821.273718.NZHERALD/B11049102.147035925;dc_trk_aid=318510830;dc_trk_cid=79731587;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=" + rand;
        else
            return "https://ad.doubleclick.net/ddm/trackimp/N7821.273718.NZHERALD/B11049102.147035925;dc_trk_aid=318510830;dc_trk_cid=79731587;ord=" + rand;
    }

	nextFrame = function(element, remove, add){
		$(element).removeClass(remove).addClass(add);
	};

	formStatus = function(message,code){
        if(code == "#10003")
        	$(".input-wrapper.email .help-block.help-message").text( message ).removeClass("hide");
        if(code == "#10004")
        	$(".input-wrapper.phone .help-block.help-message").text( message ).removeClass("hide");
    }

    errorHighlight = function(error){
        switch(error) {
            case '#10003':
                highlightGeneric("input#email");
                break;
            case '#10004':
                highlightGeneric("input#phone");
                break;
        }
    }

    highlightGeneric = function(classes){
        $(classes).addClass('error');
    }

    sandAnimate = function(){
        $('#sand-top img').addClass('fall');
        $('#sand-bottom img').addClass('rise');
        track('Frames','Frame 3 - Sand Machine', 'Animation Started');
    }

    frameOne = function(){
        track('Frames','Frame 1', 'Animations Started');
        nextFrame('.frameOne .top','hide','');
        nextFrame('.frameOne .machine img','invisible','');
        nextFrame('footer.footer','hide','slideInUp animated');
        $('footer div.invisible').remove();
    }

    $("input, select").focus(function(a) {
        $(a.target).removeClass("error");
        $(".help-block.help-message").addClass("hide alert-danger");
    });

    $(document).on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function (a) {
        $target = $(a.target);
        if($target.hasClass('circle-2')){
            nextFrame('.top.Right img','hide','zoomIn');
        }
        if($target.hasClass('topText')){
            track('Frames','Frame 1', 'Animations Complete');
            setTimeout(function(){
                track('Frames','Frame 2 - Form', 'Loaded');
                nextFrame('.footer .footer-top','','invisible');
                nextFrame('.footer .text','','center');
                nextFrame('.frame.frameTwo','','active');
            },3500);
        }
        if($target.hasClass('rise')){
            setTimeout(function(){
                track('Frames','Frame 3 - Sand Machine', 'Animation Complete');
                nextFrame('.frame.frameFour','active','off');
                track('Frames','Frame 4 - Last Frame', 'Loaded');
            },500);
        }
        if($target.hasClass('zoomOut')){
            nextFrame('.overlay .right','','overlayOpenRight');
            nextFrame('.overlay .left','','overlayOpenLeft');
            track('Frames','Unloading Complete', 'Prealoder');
            setTimeout(function(){
                frameOne();
            },500);
        }
        if($target.hasClass('overlayOpenLeft')){
            $('.overlay').remove();
        }
    });

   $(document).on("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend",function(a){
        $target = $(a.target);

        if($target.hasClass('frameTwo'))
            if($target.hasClass('off'))
                sandAnimate();
   });
});